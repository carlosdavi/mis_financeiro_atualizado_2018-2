package br.com.genesis.principal;

import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.mis.dao.DiscipuloDao;
import br.com.mis.model.DiscipuloModel;

@Path("/lider")
//@ApplicationPath("rest")
public class LiderServico {

	
	@GET//@POST//@GET
	@Path("/list")
	//@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<DiscipuloModel> listarLideres() throws SQLException
	{	
		
		DiscipuloModel discipulo = new DiscipuloModel();
		discipulo.seteLider("S");
		discipulo.setAtivo("S");
		
		return DiscipuloDao.pesquisarDiscipulos(discipulo);
	}
	
}
