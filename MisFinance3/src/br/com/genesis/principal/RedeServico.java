package br.com.genesis.principal;

import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.mis.dao.GeracaoDao;
import br.com.mis.dao.RedeDao;
import br.com.mis.model.GeracaoModel;
import br.com.mis.model.RedeModel;
@Path("/rede")
public class RedeServico {

	
	@GET
	@Path("/list")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<RedeModel> listarRedes() throws SQLException
	{			
		return RedeDao.pesquisarRedes();
	}
	
	@POST
	@Path("/insert")
	@Consumes(MediaType.APPLICATION_JSON)
	public String inserirRede(RedeModel rede)
	{				
		try
		{	
			RedeDao.inserirRede(rede);
			return "OK";
		} catch(Exception e){
			return "Erro ao inserir Discipulo";		
		}	
	}
	
}
