package br.com.genesis.principal;

import java.util.Calendar;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.mis.dao.CaixaDao;
import br.com.mis.dao.DiscipuloDao;
import br.com.mis.dao.MovimentacaoDao;
import br.com.mis.model.CaixaModel;
import br.com.mis.model.DiscipuloModel;
import br.com.mis.model.MovimentacaoModel;
import br.com.mis.to.CaixaTo;
@Path("/movimentacao")
public class CaixaOperacoes {
	
	
	@POST
	@Path("/retirada")
	@Consumes(MediaType.APPLICATION_JSON)
	public String RetiradaCaixa(MovimentacaoModel movimentacao)
	{	try
		{
			CaixaTo to = new CaixaTo();
			to.setValor(movimentacao.getValor());
			CaixaDao.retirarCaixa(to);
			MovimentacaoDao.inserirMovimentacao(movimentacao);
			return "OK";
		} catch(Exception e){
			return "Erro ao realizar retirada";		
		}	
	}
	@POST
	@Path("/entrada")
	@Consumes(MediaType.APPLICATION_JSON)
	public String EntradaMovimentacao(MovimentacaoModel movimentacao)
	{	try
		{
			MovimentacaoDao.inserirMovimentacao(movimentacao);
			return "OK";
		} catch(Exception e){
			return "Erro ao realizar retirada";		
		}	
	}
	@GET
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	public CaixaModel getCaixa()
	{	try
		{	
			return CaixaDao.getCaixa();
			
		} catch(Exception e){
			System.out.println(e.getMessage());	
			return null;
		}	
	}

}
