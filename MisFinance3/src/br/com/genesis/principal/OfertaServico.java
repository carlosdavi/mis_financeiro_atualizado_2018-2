package br.com.genesis.principal;

import java.util.Calendar;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.mis.dao.DiscipuloDao;
import br.com.mis.dao.DizimoDao;
import br.com.mis.dao.OfertaDao;
import br.com.mis.model.DiscipuloModel;
import br.com.mis.model.DizimoModel;
import br.com.mis.model.OfertaModel;
import util.Filtros;

@Path("/oferta")
public class OfertaServico {

	@POST//@GET
	@Path("/insert")
	@Consumes(MediaType.APPLICATION_JSON)
	public String inserirOferta(OfertaModel oferta)
	{
		
		
		try
		{	
			OfertaDao.inserirOferta(oferta);
			return "OK";
		} catch(Exception e){
			return "Erro ao inserir Oferta";		
		}	
	}
	
	@GET 
	@Path("/list")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<OfertaModel> listarOfertas(){ //O objeto filtros serenviar algum filtro que nao tenha dentro do objeto Dizimo

		return OfertaDao.pesquisarOfertas();
	}

}
