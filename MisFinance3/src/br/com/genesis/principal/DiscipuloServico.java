package br.com.genesis.principal;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
//import javax.ws.rs.ApplicationPath;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter.FixedSpaceIndenter;

import Testes.Teste;
import br.com.mis.dao.DiscipuloDao;
import br.com.mis.model.DiscipuloModel;
import br.com.mis.to.DiscipuloTo;

@Path("/discipulo")
//@ApplicationPath("rest")
public class DiscipuloServico {

@POST
@Path("/insert")
@Consumes(MediaType.APPLICATION_JSON)
public String inserirDiscipulo(DiscipuloModel discipulo)
{
	//DiscipuloModel discipulo = new DiscipuloModel();
	//discipulo.setNomeDiscipulo("Camila Araujo Costa Lira");
	//discipulo.setBairro("araturi");
	//Calendar calendar  = Calendar.getInstance();
	//calendar.set(2018, 01,26);
	//discipulo.setDataEntrada(calendar);
	
	try
	{	
		DiscipuloDao.inserirDiscipulo(discipulo);
		return "OK";
	} catch(Exception e){
		return "Erro ao inserir Discipulo";		
	}	
}

@POST
@Path("/update")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public String AtualizarDiscipulo(DiscipuloModel discipulo)
{
			
	EntityManagerFactory entityManagerFactory = Persistence
            .createEntityManagerFactory("MisFinance2");
	EntityManager em = entityManagerFactory.createEntityManager();
	em.getTransaction().begin();
	DiscipuloModel discipuloBanco = em.find(DiscipuloModel.class, discipulo.getId());
		
	//Verificar quais campos foram alterados	
	if(discipulo.getAtivo() != null && discipulo.getAtivo() != discipuloBanco.getAtivo())
		discipuloBanco.setAtivo(discipulo.getAtivo());
	if(discipulo.getBairro() != null && discipulo.getBairro() != discipuloBanco.getBairro())
		discipuloBanco.setBairro(discipulo.getBairro());
	if(discipulo.getCidade() != null && discipulo.getCidade() != discipuloBanco.getCidade())
		discipuloBanco.setCidade(discipulo.getCidade());
	if(discipulo.getCodGeracao() != null && discipulo.getCodGeracao() != discipuloBanco.getCodGeracao())
		discipuloBanco.setCodGeracao(discipulo.getCodGeracao());
	if(discipulo.getCodLider() != null && discipulo.getCodLider() != discipuloBanco.getCodLider())
		discipuloBanco.setCodLider(discipulo.getCodLider());
	if(discipulo.geteCoLider() != null && discipulo.geteCoLider() != discipuloBanco.geteCoLider())
		discipuloBanco.seteCoLider(discipulo.geteCoLider());
	if(discipulo.geteLider() != null && discipulo.geteLider() != discipuloBanco.geteLider())
		discipuloBanco.seteLider(discipulo.geteLider());
	if(discipulo.getEndereco() != null && discipulo.getEndereco() != discipuloBanco.getEndereco())
		discipuloBanco.setEndereco(discipulo.getEndereco());
	if(discipulo.getNomeDiscipulo() != null && discipulo.getNomeDiscipulo() != discipuloBanco.getNomeDiscipulo())
		discipuloBanco.setNomeDiscipulo(discipulo.getNomeDiscipulo());
	if(discipulo.getTelefone() != null && discipulo.getTelefone() != discipuloBanco.getTelefone())
		discipuloBanco.setTelefone(discipulo.getTelefone());
	if(discipulo.getTrabalha() != null && discipulo.getTrabalha() != discipuloBanco.getTrabalha())
		discipuloBanco.setTrabalha(discipulo.getTrabalha());
	
	if(discipulo.getDataRegistro() != null && discipulo.getDataRegistro() != discipuloBanco.getDataRegistro())
		discipuloBanco.setDataRegistro(discipulo.getDataRegistro());
	/*if(discipulo.getDataNascimento() != null && discipulo.getDataNascimento() != discipuloBanco.getDataNascimento())
		discipuloBanco.setDataNascimento(discipulo.getDataNascimento());
	if(discipulo.getDataEntrada() != null && discipulo.getDataEntrada() != discipuloBanco.getDataEntrada())
		discipuloBanco.setDataEntrada(discipulo.getDataEntrada());*/
	if(discipulo.getFk_idRede() != null && discipulo.getFk_idRede() != discipuloBanco.getFk_idRede()) {
		discipuloBanco.setFk_idRede(discipulo.getFk_idRede());
	}
	em.getTransaction().commit();
	em.close();
	entityManagerFactory.close();
	
	return null;
}

@GET
@Path("/get")
@Produces(MediaType.APPLICATION_JSON)
public DiscipuloModel GetDiscipulo(/*DiscipuloModel discipulo*/)
{	
	DiscipuloModel discipulo = new DiscipuloModel();
	discipulo.setId(1);
	return DiscipuloDao.getDiscipulo(discipulo.getId());
}

@POST//@GET
@Path("/list")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public List<DiscipuloModel> listarDiscipulos(DiscipuloModel discipulo) throws SQLException
{
	
	
	//DiscipuloModel discipuloTrste = new DiscipuloModel();
	//discipuloTrste.setBairro("araturi");
	//discipuloTrste.setAtivo("S");
	//Calendar calendar = Calendar.getInstance();
	//calendar.set(2018,01,26);
	//discipuloTrste.setDataEntrada(calendar);		
	
	return DiscipuloDao.pesquisarDiscipulos(discipulo);
}

@POST
@Path("/delete")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public String ApagarDiscipulo(DiscipuloTo discipulo)
{
	
	return null;
}
@GET
@Path("/countUltimosDiscipulosRegistrados")
@Produces(MediaType.APPLICATION_JSON)
public ArrayList<Long> CountUltimosDiscipulos()
{
	//Esse servi�o dever� retornar  n�mero de discipulos registrados nos ultimos 6 meses 	
	
	try {
		return DiscipuloDao.CountDiscipulosAno();
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		System.out.println("Houve algum problema  e a consulta n�o foi executada. "+e.getMessage());
	}
	return null;
}

private static DiscipuloModel CopiarMudancas(DiscipuloModel discipuloAtual, DiscipuloModel discipuloACopiar) {
	
	return null;
}	


}
