package br.com.genesis.principal;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.mis.dao.DiscipuloDao;
import br.com.mis.dao.DizimoDao;
import br.com.mis.dao.MovimentacaoDao;
import br.com.mis.model.DiscipuloModel;
import br.com.mis.model.DizimoModel;
import br.com.mis.model.MovimentacaoModel;
import util.Filtros;

@Path("/dizimo")
public class DizimoServico {

	@POST //--> Aqui deve ser um @POST, pois recebera um objeto do front
	@Path("/insert")
	@Consumes(MediaType.APPLICATION_JSON)
	public String inserirDizimo(DizimoModel dizimoObject)
	{		
		return DizimoDao.inserirDizimo(dizimoObject);
	}
	@GET //@POST
	@Path("/list")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<DizimoModel> listarDizimistas(/*DizimoModel dizimoObject, Filtros filtros*/){ //O objeto filtros serenviar algum filtro que nao tenha dentro do objeto Dizimo

		DizimoModel dizimoObject = new DizimoModel();
		//dizimoObject.setFk_idDiscipulo(3);
		Filtros filtro = new Filtros();				
		return DizimoDao.pesquisarDizimos(null,null);

	}
}
