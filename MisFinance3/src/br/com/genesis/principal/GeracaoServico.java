package br.com.genesis.principal;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.mis.dao.DiscipuloDao;
import br.com.mis.dao.GeracaoDao;
import br.com.mis.model.DiscipuloModel;
import br.com.mis.model.GeracaoModel;

@Path("/geracao")
public class GeracaoServico {

	@GET
	@Path("/list")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<GeracaoModel> listarGeracoes() throws SQLException
	{			
		return GeracaoDao.pesquisarGeracoes();
	}
	
	@POST
	@Path("/insert")
	@Consumes(MediaType.APPLICATION_JSON)
	public String inserirGeracao(GeracaoModel geracao)
	{				
		try
		{	
			GeracaoDao.inserirGeracao(geracao);
			return "OK";
		} catch(Exception e){
			return "Erro ao inserir Discipulo";		
		}	
	}
	
	
}
