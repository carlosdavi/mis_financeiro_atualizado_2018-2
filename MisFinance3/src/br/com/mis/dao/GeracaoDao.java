package br.com.mis.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import br.com.mis.model.GeracaoModel;
import br.com.mis.model.OfertaModel;

public class GeracaoDao {

	public static String inserirGeracao(GeracaoModel geracaoObject){
		try{
			if(GeracaoDao.validarNotNulls(geracaoObject)){
				EntityManagerFactory entityManagerFactory = Persistence
			            .createEntityManagerFactory("MisFinance3");	
				EntityManager em = entityManagerFactory.createEntityManager();			
				em.getTransaction().begin();
				em.persist(geracaoObject);
				em.getTransaction().commit();
				em.close();
				entityManagerFactory.close();
				return "Operacaoo realizada com sucesso.";
			}
			else{
				return "Houve um problema: Verificar se todos os campos est�o preenchidos.";
			}
			
		}
		catch(Exception e){
			e.printStackTrace();
			return "Houve um problema durante a insercao da informacao";
		}
	}
	
public static List<GeracaoModel> pesquisarGeracoes(){
		
		try{
		EntityManagerFactory entityManagerFactory = Persistence
	            .createEntityManagerFactory("MisFinance3");		
		String jpql = gerarQueyFiltradoGeracao(null);
		EntityManager em = entityManagerFactory.createEntityManager();		
		Query query = em.createQuery(jpql);		
		
		em.getTransaction().begin();
		List<GeracaoModel> lista  = query.getResultList();
		List<GeracaoModel> lista2 = new ArrayList<>();
		for (GeracaoModel object : lista) {
			lista2.add(object);
		}
		em.getTransaction().commit();
		em.close();
		entityManagerFactory.close();
		
		return lista2;
		}
		catch(Exception e){
			System.out.println("------------------------- Erro encontrado ----------------------------------");
			e.printStackTrace();
			return null;
		}
		
		
	}

/* POR HORA ESSE METODO N�O PRECISA DE FILTROS, POIS N�O � NECESS�RIO SABER QUEM ESTA DANDO A OFERTA.*/
public static String gerarQueyFiltradoGeracao(GeracaoModel model) {
	String jpql = null;
	if(model == null)
		jpql = "select m from geracao m ";
	
	return jpql;
	
}
	
	

	private static boolean validarNotNulls(GeracaoModel geracaoObject){
		if(geracaoObject == null)
			return false;
		else if(geracaoObject.getCodLiderGeracao() == null)
			return false;
		else if(geracaoObject.getNomeGeracao() == null)
			return false;		
		else
			return true;		
	}
}
