package br.com.mis.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import br.com.mis.model.DiscipuloModel;
import br.com.mis.model.DizimoModel;
import br.com.mis.model.MovimentacaoModel;
import br.com.mis.to.CaixaTo;
import util.Filtros;

public class MovimentacaoDao {

	

	public static String inserirMovimentacao(MovimentacaoModel movimentacao){
		try{
			EntityManagerFactory entityManagerFactory = Persistence
			            .createEntityManagerFactory("MisFinance3");	
				EntityManager em = entityManagerFactory.createEntityManager();			
				em.getTransaction().begin();
				em.persist(movimentacao);		
				em.getTransaction().commit();				
				em.close();
				entityManagerFactory.close();
				return "Operacaoo realizada com sucesso.";
		}
		catch(Exception e){
			e.printStackTrace();
			
			return "Houve um problema durante a insercao da informacao";
		}
	}
	
public static List<MovimentacaoModel> pesquisarPesquisarMovimentacao(DizimoModel dizimoObject,Filtros filtro){
		
		try{
		EntityManagerFactory entityManagerFactory = Persistence
	            .createEntityManagerFactory("MisFinance3");		
	//	String jpql = gerarQueyFiltradoDizimo(dizimoObject,filtro);
		EntityManager em = entityManagerFactory.createEntityManager();		
		//Query query = em.createQuery(jpql);
		Query query = em.createQuery("select m from movimentacao m");
		MovimentacaoModel movimentacao = new MovimentacaoModel();
		//discipulo.setId(dizimoObject.getFk_idDiscipulo());
		//query.setParameter("idDiscipulo", discipulo.getId());
		em.getTransaction().begin();		
		query.getResultList();
		List<MovimentacaoModel> lista  = query.getResultList();
		List<MovimentacaoModel> lista2 = new ArrayList<MovimentacaoModel>();
	
		em.getTransaction().commit();
		em.close();
		entityManagerFactory.close();
		
		return lista;
		}
		catch(Exception e){
			System.out.println("------------------------- Erro encontrado ----------------------------------");
			System.out.println(e.getMessage());
			return null;
		}
		
		
	}

private static String gerarQueyFiltradoDizimo(DizimoModel dizimoObject, Filtros filtro) {
	// TODO Auto-generated method stub
	return null;
}	
	
}
