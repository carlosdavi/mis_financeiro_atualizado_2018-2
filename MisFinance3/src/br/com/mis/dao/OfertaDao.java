package br.com.mis.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import br.com.mis.model.DizimoModel;
import br.com.mis.model.OfertaModel;
import br.com.mis.to.CaixaTo;
import util.Filtros;

public class OfertaDao {

	public static String inserirOferta(OfertaModel ofertaObject){
		try{
			if(OfertaDao.validarNotNulls(ofertaObject)){
				EntityManagerFactory entityManagerFactory = Persistence
			            .createEntityManagerFactory("MisFinance3");	
				EntityManager em = entityManagerFactory.createEntityManager();			
				em.getTransaction().begin();
				em.persist(ofertaObject);
				em.getTransaction().commit();
				em.close();
				entityManagerFactory.close();
				CaixaTo caixa = new CaixaTo();
				caixa.setValor(ofertaObject.getValorOferta());				
				CaixaDao.atribuirCaixa(caixa);
				return "Operacaoo realizada com sucesso.";
			}
			else{
				return "Houve um problema: Verificar se todos os campos est�o preenchidos.";
			}
			
		}
		catch(Exception e){
			e.printStackTrace();
			return "Houve um problema durante a insercao da informacao";
		}
	}
	
public static List<OfertaModel> pesquisarOfertas(){
		
		try{
		EntityManagerFactory entityManagerFactory = Persistence
	            .createEntityManagerFactory("MisFinance3");		
		String jpql = gerarQueyFiltradoDizimo(new OfertaModel());
		EntityManager em = entityManagerFactory.createEntityManager();		
		Query query = em.createQuery(jpql);		
		
		em.getTransaction().begin();
		List<OfertaModel> lista  = query.getResultList();
		List<OfertaModel> lista2 = new ArrayList<>();
		for (OfertaModel object : lista) {
			lista2.add(object);
		}
		em.getTransaction().commit();
		em.close();
		entityManagerFactory.close();
		
		return lista2;
		}
		catch(Exception e){
			System.out.println("------------------------- Erro encontrado ----------------------------------");
			e.printStackTrace();
			return null;
		}
		
		
	}

/* POR HORA ESSE METODO N�O PRECISA DE FILTROS, POIS N�O � NECESS�RIO SABER QUEM ESTA DANDO A OFERTA.*/
public static String gerarQueyFiltradoDizimo(OfertaModel model) {
	
	String jpql = "select m from ofertas m ";
	
	return jpql;
	
}

public static ArrayList<Long> CountOfertaAno(){
	Calendar dataReferencia = Calendar.getInstance();
	ArrayList<Filtros> listaFiltros = new ArrayList<Filtros>();
	//int mesAtual = dataReferencia.getMonth();
	int anoAtual = dataReferencia.getTime().getMonth()+1900;
	Calendar dataFim = Calendar.getInstance();
	Calendar dataInicio = Calendar.getInstance();
	for(int i = 0; i <= 11;i++ ) {	
		Filtros filtro = new Filtros();	
		filtro.setDataFim(dataFim);
		filtro.setDataInicio(dataInicio);		
		filtro.setDataFimDate(new Date(118,i,31));		
		filtro.setDataInicioDate(new Date(118,i,1));		
		listaFiltros.add(filtro);		
	}
	ArrayList<Long> listaRetorno = new ArrayList<>();
	EntityManagerFactory entityManagerFactory = Persistence
            .createEntityManagerFactory("MisFinance3");	
	EntityManager em = entityManagerFactory.createEntityManager();		
	String sql = "select count(d) from ofertas d where d.dataOferta between :data1 and :data2";
	Query query = em.createQuery(sql);
	for(int i = 0; i <= 11;i++) {
		try{
			query.setParameter("data1", listaFiltros.get(i).getDataInicioDate());				
			query.setParameter("data2", listaFiltros.get(i).getDataFimDate());				
			em.getTransaction().begin();
			Long retornoQuery  =  (long) query.getSingleResult();
			listaRetorno.add(retornoQuery);				
			em.getTransaction().commit();
			
		}
			catch(Exception e){
				System.out.println("------------------------- Erro encontrado ----------------------------------");
				System.out.println(e.getMessage());
				return null;
			}			
}
	em.close();
	entityManagerFactory.close();

	return listaRetorno;	
}

public static ArrayList<Long> MedOfertaAno(){
	Calendar dataReferencia = Calendar.getInstance();
	ArrayList<Filtros> listaFiltros = new ArrayList<Filtros>();
	//int mesAtual = dataReferencia.getMonth();
	int anoAtual = dataReferencia.getTime().getMonth()+1900;
	Calendar dataFim = Calendar.getInstance();
	Calendar dataInicio = Calendar.getInstance();
	for(int i = 0; i <= 11;i++ ) {	
		Filtros filtro = new Filtros();	
		filtro.setDataFim(dataFim);
		filtro.setDataInicio(dataInicio);		
		filtro.setDataFimDate(new Date(118,i,31));		
		filtro.setDataInicioDate(new Date(118,i,1));		
		listaFiltros.add(filtro);		
	}
	ArrayList<Long> listaRetorno = new ArrayList<>();
	EntityManagerFactory entityManagerFactory = Persistence
            .createEntityManagerFactory("MisFinance3");	
	EntityManager em = entityManagerFactory.createEntityManager();		
	String sql = "select IFNULL(avg(d.valorOferta),0) from ofertas d where d.dataOferta between :data1 and :data2";
	Query query = em.createQuery(sql);
	for(int i = 0; i <= 11;i++) {
		try{
			query.setParameter("data1", listaFiltros.get(i).getDataInicioDate());				
			query.setParameter("data2", listaFiltros.get(i).getDataFimDate());				
			em.getTransaction().begin();
			Long retornoQuery  =  (long) query.getSingleResult();
			listaRetorno.add(retornoQuery);				
			em.getTransaction().commit();
			
		}
			catch(Exception e){
				System.out.println("------------------------- Erro encontrado ----------------------------------");
				System.out.println(e.getMessage());
				return null;
			}			
}
	em.close();
	entityManagerFactory.close();

	return listaRetorno;	
}

	
	

	private static boolean validarNotNulls(OfertaModel ofertaObject){
		if(ofertaObject == null)
			return false;
		else if(ofertaObject.getValorOferta() == null)
			return false;
		else if(ofertaObject.getOcasiao() == null)
			return false;		
		else
			return true;		
	}
	
}
