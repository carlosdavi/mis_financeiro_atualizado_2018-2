package br.com.mis.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import br.com.mis.model.DiscipuloModel;
import br.com.mis.model.DizimoModel;
import br.com.mis.to.CaixaTo;
import br.com.mis.to.DizimoTo;
import util.Filtros;

public class DizimoDao {
	
	public static String inserirDizimo(DizimoModel dizimoObject){
		try{
			//if(DizimoDao.validarNotNulls(dizimoObject)){
				EntityManagerFactory entityManagerFactory = Persistence
			            .createEntityManagerFactory("MisFinance3");	
				EntityManager em = entityManagerFactory.createEntityManager();			
				CaixaTo caixaTo = new CaixaTo();
				caixaTo.setRazao("DIZIMO");
				caixaTo.setTipoMovimentacao("ENTRADA");
				caixaTo.setValor(dizimoObject.getValorDizimo());
				em.getTransaction().begin();
				em.persist(dizimoObject);		
				em.getTransaction().commit();				
				CaixaDao.atribuirCaixa(caixaTo);
				em.close();
				entityManagerFactory.close();
				return "Operacaoo realizada com sucesso.";
			//}
			//else{
			//	return "Houve um problema: Verificar se todos os campos est�o preenchidos.";
			//}
			
		}
		catch(Exception e){
			e.printStackTrace();
			
			return "Houve um problema durante a insercao da informacao";
		}
	}
	
public static List<DizimoModel> pesquisarDizimos(DizimoModel dizimoObject,Filtros filtro){
		
		try{
		EntityManagerFactory entityManagerFactory = Persistence
	            .createEntityManagerFactory("MisFinance3");		
		String jpql = gerarQueyFiltradoDizimo(dizimoObject,filtro);
		EntityManager em = entityManagerFactory.createEntityManager();		
		Query query = em.createQuery(jpql);
		//Query query = em.createQuery("select m from dizimo m join fetch m.discipulo where m.discipulo.id=:idDiscipulo");
		DiscipuloModel discipulo = new DiscipuloModel();
		//discipulo.setId(dizimoObject.getFk_idDiscipulo());
		//query.setParameter("idDiscipulo", discipulo.getId());
		em.getTransaction().begin();		
		query.getResultList();
		List<DizimoModel> lista  = query.getResultList();
		List<DizimoModel> lista2 = new ArrayList<DizimoModel>();
	
		em.getTransaction().commit();
		em.close();
		entityManagerFactory.close();
		
		return lista;
		}
		catch(Exception e){
			System.out.println("------------------------- Erro encontrado ----------------------------------");
			System.out.println(e.getMessage());
			return null;
		}
		
		
	}
	
public static String gerarQueyFiltradoDizimo(DizimoModel model, Filtros filtro) {
	
	String jpql = "select m from dizimos m";
	if(model == null && filtro == null)
		//return "select m from dizimos m inner join discipulos d on m.fk_idDiscipulo = d.id";
		return "select m from dizimos m";
	
	if(model.getFk_idDiscipulo() != null && !model.getFk_idDiscipulo().equals(0)){
		//jpql = "select m from dizimos m inner join discipulos d on m.fk_idDiscipulo = d.id";
		jpql = "select m from dizimos m";

	}
	jpql = jpql + " where ";
	if(model.getOcasiao() != null && !model.getOcasiao().equals("")){
		jpql = jpql+" and m.ocasiao ='"+model.getOcasiao()+"'";
	}
	if(model.getValorDizimo() != null && !model.getValorDizimo().equals(0)){
		jpql = jpql+" and m.valorDizimo ="+model.getValorDizimo();
	}
	if(filtro != null){
		if((filtro.getDataInicio() != null && !filtro.getDataInicio().equals("")) && (filtro.getDataFim() != null && !filtro.getDataFim().equals(""))){
			jpql = jpql+" and m.dataEntrega >= '"+filtro.getDataInicio().getInstance()+"' and m.dataEntrega <= "+filtro.getDataFim().getInstance();
		}
		else if((filtro.getDataInicio() != null && !filtro.getDataInicio().equals("")) && (filtro.getDataFim() == null || filtro.getDataFim().equals(""))){
			jpql = jpql+" and m.dataEntrega >= '"+filtro.getDataInicio();
		}
		else if((filtro.getDataInicio() == null || filtro.getDataInicio().equals("")) && (filtro.getDataFim() != null && !filtro.getDataFim().equals(""))){
			jpql = jpql+"' and m.dataEntrega <= "+filtro.getDataFim();
		}
	}
	else if(model.getDataEntrega() != null && !model.getDataEntrega().equals("")){
		jpql = jpql+"' and m.dataEntrega <= "+model.getDataEntrega();
	}
	if(model.getFk_idDiscipulo() != null && !model.getFk_idDiscipulo().equals(0)){
		jpql = jpql+ "d.id ="+model.getFk_idDiscipulo();
	}
	return jpql;
	
}

public static ArrayList<Long> CountDizimosAno(){
	Calendar dataReferencia = Calendar.getInstance();
	ArrayList<Filtros> listaFiltros = new ArrayList<Filtros>();
	//int mesAtual = dataReferencia.getMonth();
	int anoAtual = dataReferencia.getTime().getMonth()+1900;
	Calendar dataFim = Calendar.getInstance();
	Calendar dataInicio = Calendar.getInstance();
	for(int i = 0; i <= 11;i++ ) {	
	/*	dataFim.set(Calendar.YEAR, dataReferencia.YEAR);
		dataFim.set(Calendar.MONTH, i);
		dataFim.set(Calendar.DAY_OF_MONTH, 31);
		//dataFim.set(dataReferencia.YEAR, i, 31);
		//dataInicio.set(dataReferencia.YEAR,i,1);
		dataInicio.set(Calendar.YEAR, dataReferencia.YEAR);
		dataInicio.set(Calendar.MONTH, i);
		dataInicio.set(Calendar.DAY_OF_MONTH, 01);*/

		Filtros filtro = new Filtros();	
		filtro.setDataFim(dataFim);
		filtro.setDataInicio(dataInicio);		
		filtro.setDataFimDate(new Date(118,i,31));		
		filtro.setDataInicioDate(new Date(118,i,1));		
		listaFiltros.add(filtro);		
	}
	ArrayList<Long> listaRetorno = new ArrayList<>();
	EntityManagerFactory entityManagerFactory = Persistence
            .createEntityManagerFactory("MisFinance3");	
	EntityManager em = entityManagerFactory.createEntityManager();		
	String sql = "select count(d) from dizimos d where d.dataEntrega between :data1 and :data2";
	Query query = em.createQuery(sql);
	for(int i = 0; i <= 11;i++) {
		try{
			query.setParameter("data1", listaFiltros.get(i).getDataInicioDate());				
			query.setParameter("data2", listaFiltros.get(i).getDataFimDate());				
			em.getTransaction().begin();
			Long retornoQuery  =  (long) query.getSingleResult();
			listaRetorno.add(retornoQuery);				
			em.getTransaction().commit();
			
		}
			catch(Exception e){
				System.out.println("------------------------- Erro encontrado ----------------------------------");
				System.out.println(e.getMessage());
				return null;
			}			
}
	em.close();
	entityManagerFactory.close();

	return listaRetorno;	
}

public static ArrayList<Long> MedDizimosAno() {
	Calendar dataReferencia = Calendar.getInstance();
	ArrayList<Filtros> listaFiltros = new ArrayList<Filtros>();
	//int mesAtual = dataReferencia.getMonth();
	int anoAtual = dataReferencia.getTime().getMonth()+1900;
	Calendar dataFim = Calendar.getInstance();
	Calendar dataInicio = Calendar.getInstance();
	for(int i = 0; i <= 11;i++ ) {	
		Filtros filtro = new Filtros();	
		filtro.setDataFim(dataFim);
		filtro.setDataInicio(dataInicio);		
		filtro.setDataFimDate(new Date(118,i,31));		
		filtro.setDataInicioDate(new Date(118,i,1));		
		listaFiltros.add(filtro);		
	}
	ArrayList<Long> listaRetorno = new ArrayList<>();
	EntityManagerFactory entityManagerFactory = Persistence
            .createEntityManagerFactory("MisFinance3");	
	EntityManager em = entityManagerFactory.createEntityManager();		
	String sql = "select IFNULL(avg(d.valorDizimo),0) from dizimos d where d.dataEntrega between :data1 and :data2";
	Query query = em.createQuery(sql);
	for(int i = 0; i <= 11;i++) {
		try{
			query.setParameter("data1", listaFiltros.get(i).getDataInicioDate());				
			query.setParameter("data2", listaFiltros.get(i).getDataFimDate());				
			em.getTransaction().begin();
			Long retornoQuery  =  (long) query.getSingleResult();
			listaRetorno.add(retornoQuery);				
			em.getTransaction().commit();
			
		}
			catch(Exception e){
				System.out.println("------------------------- Erro encontrado ----------------------------------");
				System.out.println(e.getMessage());
				return null;
			}			
}
	em.close();
	entityManagerFactory.close();

	return listaRetorno;
}
	
	private static boolean validarNotNulls(DizimoModel dizimoObjetc){
		if(dizimoObjetc.getDataEntrega() == null)
			return false;
		else if(dizimoObjetc.getFk_idDiscipulo() == null)
			return false;
		else if(dizimoObjetc.getValorDizimo() == null)
			return false;
		else
			return true;		
	}

	

}
