package br.com.mis.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;
@Entity(name="rede")
public class RedeModel {

@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
private Integer idRede;	
private String nomeRede;
private Integer id_fkLiderRede;
@Transient 
private String nomeLider;






public String getNomeLider() {
	return nomeLider;
}
public void setNomeLider(String nomeLider) {
	this.nomeLider = nomeLider;
}
public Integer getIdRede() {
	return idRede;
}
public void setIdRede(Integer idRede) {
	this.idRede = idRede;
}
public String getNomeRede() {
	return nomeRede;
}
public void setNomeRede(String nomeRede) {
	this.nomeRede = nomeRede;
}
public Integer getId_fkLiderRede() {
	return id_fkLiderRede;
}
public void setId_fkLiderRede(Integer id_fkLiderRede) {
	this.id_fkLiderRede = id_fkLiderRede;
}




}
