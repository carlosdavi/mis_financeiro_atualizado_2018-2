package br.com.mis.model;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name="geracao")
public class GeracaoModel {	
	

		@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
		private Integer id;			
		private Integer codLiderGeracao;	
		private String nomeGeracao;
		
		
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public Integer getCodLiderGeracao() {
			return codLiderGeracao;
		}
		public void setCodLiderGeracao(Integer codLiderGeracao) {
			this.codLiderGeracao = codLiderGeracao;
		}
		public String getNomeGeracao() {
			return nomeGeracao;
		}
		public void setNomeGeracao(String nomeGeracao) {
			this.nomeGeracao = nomeGeracao;
		}
		
		
}
