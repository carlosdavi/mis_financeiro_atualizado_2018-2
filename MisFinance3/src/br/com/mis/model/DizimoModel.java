package br.com.mis.model;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import br.com.mis.to.DiscipuloTo;

@Entity(name="dizimos")
public class DizimoModel {
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idDizimo;	
	private Double valorDizimo;	
	@Temporal(TemporalType.DATE)
	private Date dataEntrega;	
	private String ocasiao;
	private Integer fk_idDiscipulo;	
	
	//@ManyToOne
	//DiscipuloModel discipulo;
	
	
	

	//Dados Do objeto Discipulos
	//@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Transient
	private Integer id;
	@Transient
	private String nomeDiscipulo;	
	@Transient
	private Integer codGeracao;
	@Transient
	private Integer codLider;
	
	
	
	
	public String getNomeDiscipulo() {
		return nomeDiscipulo;
	}

	public void setNomeDiscipulo(String nomeDiscipulo) {
		this.nomeDiscipulo = nomeDiscipulo;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCodGeracao() {
		return codGeracao;
	}

	public void setCodGeracao(Integer codGeracao) {
		this.codGeracao = codGeracao;
	}

	public Integer getCodLider() {
		return codLider;
	}

	public void setCodLider(Integer codLider) {
		this.codLider = codLider;
	}

public Integer getIdDizimo() {
		return idDizimo;
	}

	public void setIdDizimo(Integer idDizimo) {
		this.idDizimo = idDizimo;
	}

	public Double getValorDizimo() {
		return valorDizimo;
	}

	public void setValorDizimo(Double valorDizimo) {
		this.valorDizimo = valorDizimo;
	}

	public Date getDataEntrega() {
		return dataEntrega;
	}

	public void setDataEntrega(Date dataEntrega) {
		this.dataEntrega = dataEntrega;
	}

	public String getOcasiao() {
		return ocasiao;
	}

	public void setOcasiao(String ocasiao) {
		this.ocasiao = ocasiao;
	}

	public Integer getFk_idDiscipulo() {
		return fk_idDiscipulo;
	}

	public void setFk_idDiscipulo(Integer fk_idDiscipulo) {
		this.fk_idDiscipulo = fk_idDiscipulo;
	}
	
	

}
