package br.com.mis.model;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name="ofertas")
public class OfertaModel {

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idOferta;	
	private Double valorOferta;	
	@Temporal(TemporalType.DATE)
	//private Calendar dataOferta;
	private Date dataOferta;
	private String ocasiao;
	
	
	public Integer getIdOferta() {
		return idOferta;
	}
	public void setIdOferta(Integer idOferta) {
		this.idOferta = idOferta;
	}
	public Double getValorOferta() {
		return valorOferta;
	}
	public void setValorOferta(Double valorOferta) {
		this.valorOferta = valorOferta;
	}
	public Date getDataOferta() {
		return dataOferta;
	}
	public void setDataOferta(Date dataOferta) {
		this.dataOferta = dataOferta;
	}
	public String getOcasiao() {
		return ocasiao;
	}
	public void setOcasiao(String ocasiao) {
		this.ocasiao = ocasiao;
	}
	
	
}
