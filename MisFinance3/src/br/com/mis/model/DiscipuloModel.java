package br.com.mis.model;

import java.util.Date;
import java.util.List;
import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name="discipulos")
public class DiscipuloModel {

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	private String nomeDiscipulo;
	private String endereco;
	private String bairro;
	private String cidade;
	private String telefone;
	private String trabalha;
	private String eLider;
	private String eCoLider;
	private String ativo;
	private Integer codLider;
	private Integer codGeracao;
	private Integer fk_idRede;
	
	
	
	@Temporal(TemporalType.DATE)
	private Date dataNascimento;
	@Temporal(TemporalType.DATE)
	private Date dataEntrada;
	//private Calendar dataEntrada;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dataRegistro;
	
	//@OneToMany(mappedBy="discipulo", fetch=FetchType.LAZY)
	//List<DizimoModel> dizimos;
	
	
	
	
	
	//public Calendar getDataEntrada() {
		//	return dataEntrada;
		//}
		public Date getDataEntrada() {
			return this.dataEntrada;
		}
	public Integer getFk_idRede() {
			return fk_idRede;
		}
		public void setFk_idRede(Integer fk_idRede) {
			this.fk_idRede = fk_idRede;
		}
	public void setDataEntrada(Date dataEntrada) {
		this.dataEntrada = dataEntrada;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNomeDiscipulo() {
		return nomeDiscipulo;
	}
	public void setNomeDiscipulo(String nomeDiscipulo) {
		this.nomeDiscipulo = nomeDiscipulo;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getTrabalha() {
		return trabalha;
	}
	public void setTrabalha(String trabalha) {
		this.trabalha = trabalha;
	}
	public String geteLider() {
		return eLider;
	}
	public void seteLider(String eLider) {
		this.eLider = eLider;
	}
	public String geteCoLider() {
		return eCoLider;
	}
	public void seteCoLider(String eCoLider) {
		this.eCoLider = eCoLider;
	}
	public String getAtivo() {
		return ativo;
	}
	public void setAtivo(String ativo) {
		this.ativo = ativo;
	}
	public Integer getCodLider() {
		return codLider;
	}
	public void setCodLider(Integer codLider) {
		this.codLider = codLider;
	}
	public Integer getCodGeracao() {
		return codGeracao;
	}
	public void setCodGeracao(Integer codGeracao) {
		this.codGeracao = codGeracao;
	}
	public Date getDataNascimento() {
		return this.dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}		
	public Calendar getDataRegistro() {
		return dataRegistro;
	}
	public void setDataRegistro(Calendar dataRegistro) {
		this.dataRegistro = dataRegistro;
	}
	
	
	
}
