package br.com.mis.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="caixa")
public class CaixaModel {
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idCaixa;	
	private Double caixa;
	
	
	public Integer getIdCaixa() {
		return idCaixa;
	}

	public void setIdCaixa(Integer idCaixa) {
		this.idCaixa = idCaixa;
	}

	public Double getCaixa() {
		return caixa;
	}

	public void setCaixa(Double caixa) {
		this.caixa = caixa;
	}
	
	

}
