package br.com.mis.to;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/*
 * O Objeto DizimoTo foi criado com o intuito de ser um objeto tempor�rio.
 * Utilizado apenas para receber informa��es oriundas de innerjoins do banco e para retonar ao front um objeto
 * em um formato n�o mapeado convencionalmente pelos os objetos de origem.
 * */
//public class DizimoTo {}
//@Entity(name="visao_dizimo_discipulo")
public class DizimoTo {
	//@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idDizimo;
	private Double valorDizimo;
	//@Temporal(TemporalType.DATE)
	private Calendar dataEntrega;
	private String ocasiao;
	private Integer fk_idDiscipulo;
	
	//Dados Do objeto Discipulos
	private String nomeDiscipulo;
	//@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	private Integer codGeracao;
	private Integer codLider;
	
	
	
	
	
	public Integer getIdDizimo() {
		return idDizimo;
	}
	public void setIdDizimo(Integer idDizimo) {
		this.idDizimo = idDizimo;
	}
	public Double getValorDizimo() {
		return valorDizimo;
	}
	public void setValorDizimo(Double valorDizimo) {
		this.valorDizimo = valorDizimo;
	}
	public Calendar getDataEntrega() {
		return dataEntrega;
	}
	public void setDataEntrega(Calendar dataEntrega) {
		this.dataEntrega = dataEntrega;
	}
	public String getOcasiao() {
		return ocasiao;
	}
	public void setOcasiao(String ocasiao) {
		this.ocasiao = ocasiao;
	}
	public Integer getFk_idDiscipulo() {
		return fk_idDiscipulo;
	}
	public void setFk_idDiscipulo(Integer fk_idDiscipulo) {
		this.fk_idDiscipulo = fk_idDiscipulo;
	}
	public String getNomeDiscipulo() {
		return nomeDiscipulo;
	}
	public void setNomeDiscipulo(String nomeDiscipulo) {
		this.nomeDiscipulo = nomeDiscipulo;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCodGeracao() {
		return codGeracao;
	}
	public void setCodGeracao(Integer codGeracao) {
		this.codGeracao = codGeracao;
	}
	public Integer getCodLider() {
		return codLider;
	}
	public void setCodLider(Integer codLider) {
		this.codLider = codLider;
	}
	
	
	

}

