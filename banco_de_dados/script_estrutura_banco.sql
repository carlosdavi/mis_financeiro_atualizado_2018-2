﻿uses database mis2018;


-- Criação da tabela de discipulos
create table discipulos(
id integer not null auto_increment primary key,
nomeDiscipulo varchar(50) not null,
eLider varchar(1) not null,
eCoLider varchar(1) not null,
ativo varchar(1) not null,
dataNascimento Date,
dataEntrada Date,
bairro varchar(30),
cidade varchar(30),
telefone varchar(15),
trabalha varchar(1),
codLider integer,
dataRegistro Datetime,
codGeracao integer
);

create table geracao (
id integer not null primary key auto_increment,
nomeGeracao varchar(30) not null,
codLiderGeracao integer
);

alter table geracao ADD CONSTRAINT codLiderGeracao 
FOREIGN KEY(codLiderGeracao) REFERENCES discipulos(id)


create table caixa(
	idCaixa integer not null auto_increment primary key,
	caixa float not null
		);

create table dizimos(
idDizimo integer not null auto_increment primary key,
valorDizimo double not null,
dataEntrega Date not null,
ocasiao varchar(30),
fk_idDiscipulo integer
);
alter table dizimos ADD CONSTRAINT fk_idDiscipulo 
FOREIGN KEY(fk_idDiscipulo) REFERENCES discipulos(id)

create table ofertas(
idOferta integer not null auto_increment primary key,
valorOferta double not null,
dataOferta Date not null,
ocasiao varchar(30)
);

create table dizimoTO(	
idDizimo integer auto_increment primary key,
valor float
);
